import NavBar from './components/NavBar';
import Home from './components/Home';
import AllAlbums from './components/AllAlbums';
import UserPage from './components/UserPage';
import { BrowserRouter as Router, Routes as Switch, Route} from 'react-router-dom' 
import Styles from './App.module.css'
import {useState} from 'react'
import { useFetchUser } from "./hooks/fetchUser";
import logo from './images/logo.jpg'; // Tell webpack this JS file uses this image
import Login from './components/Login';


function App() {

  const [userInput, setUserInput] = useState('')
  const [userInputButton, setUserserInputButton] = useState('')
  const [usersList, setUsersList] = useState([])
  
  useFetchUser(setUsersList)

  const userPagesRoutes = []

  for (let i = 1; i <= usersList.length; i++) {
    userPagesRoutes.push(<Route path={`/userPage/${i}`} element={<UserPage name={usersList[i-1].name} username={usersList[i-1].username} userId={usersList[i-1].id} email={usersList[i-1].email} />}/>)
  }
  

  return (
    <Router>
      <div >
        <div className={Styles.headWrapper}>
          <h1 ><img source={logo} />Amit<span>Stegram</span></h1>
          {userInputButton.length !== 0 && <h1>Hello {userInputButton}</h1>}
          <NavBar />
        </div>
        <div className={Styles.pageWrapper}>
          <Switch>
              <Route path='/' element= {<Login userInput={userInput} setUserInput={setUserInput} setUserserInputButton={setUserserInputButton}/>}/>
              <Route path='/home' element= {<Home usersList={usersList}/>}/>
              <Route path='/allAlbums' element= {<AllAlbums />}/>
              {userPagesRoutes}
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
