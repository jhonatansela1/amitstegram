import User from "../User/User"




const Home = ({usersList}) => {

    const userLinks = (
        usersList.map((element, index) => {
            
            return (
                    <div key={index}>
                        <User userName={element.username} userId={element.id} />
                    </div>
            )}))
    
    return (
        <div >
            <h1>Pick A User</h1>
            {userLinks}

        </div>
    )
}

export default Home