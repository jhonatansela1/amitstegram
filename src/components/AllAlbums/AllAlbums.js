import { useState } from 'react'
import { useFetchAllAlbums } from '../../hooks/fetchAllAlbums'
import Styles from './AllAlbums.module.css'
import Album from '../Album'
const AllAlbums = () =>{

    const [allAlbums, setAllAlbums] = useState([])

    useFetchAllAlbums(setAllAlbums)

    const allAlbumsShow = (
        allAlbums.map((element, index) => {  
            return (
                    <div key={index}>
                        <Album id={element.id} title={element.title} />    
                    </div>
            )
        })
    )

    return (
        <div className={Styles.divi}>
            <h2>Select An Album From All The Albums</h2>
            {allAlbumsShow}
        </div>
    )
}

export default AllAlbums