import Styles from './Login.module.css'
import logo from '../../images/logo.jpg'
import { useState } from 'react'
import { Navigate } from 'react-router-dom'

const Login = ({userInput , setUserInput, setUserserInputButton}) => {
    const [isValid, setIsValid] = useState(' ')
    
    const [passInput, setPassInput] = useState('')

    const handleSignIn = () => {
        if (userInput.length !== 0 && passInput.length !== 0){
            setIsValid('')
            setUserserInputButton(userInput)
        }
        else{
            setIsValid('Please Enter Username And Password!')
        }
    }

    const handleUserChange = (value) => {
        setUserInput(value)
        console.log(userInput)
    }
    const handlePassChange = (value) => {
        setPassInput(value)
        console.log(passInput)
    }
    
    return (
        <div className={Styles.loginWrapper}>
            Username: <input onChange={(event) => handleUserChange(event.target.value)}/>
            Password: <input type='password' onChange={(event) => handlePassChange(event.target.value)}/>
            <button onClick={handleSignIn}>Sign In</button>
            {isValid.length === 0 ? <Navigate to='/home'/> : <span className={Styles.errorMess}>{isValid}</span>}
            <img src={logo}/>
        </div>
    )
}

export default Login