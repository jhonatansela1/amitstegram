import { NavLink } from "react-router-dom"
import Styles from './User.module.css'

const User = (props) => {

    const {userName, userId, name, email}= props
    return (
        <div className={Styles.usersLinks}>
            <NavLink to={`/userPage/${userId}`} className={Styles.shobby}>{userName}</NavLink>
        </div>
    )
}

export default User
