import Styles from './Photo.module.css'



const Photo = ({url, title}) => {
    return (
        <div className={Styles.photoWrapper}>
            <h1>{title}</h1>
            <br/>
            <img src={url} />
            <hr />
        </div>
    )
}

export default Photo