import { useFetchAlbums } from "../../hooks/fetchAlbums"
import { useState } from "react";
import Album from "../Album";

const UserPage= (props) => {

    const {userName, userId, name, email}= props
    

    const [albumsList, setAlbumsList] = useState([]);
    const [whoShown, setWhoShown] = useState('');

    useFetchAlbums(userId, setAlbumsList)

    const albumLinks = (
        albumsList.map((element, index) => {
            
            return (
                    <div key={index}>
                        <Album id={element.id} title={element.title} whoShown={whoShown} />
                    </div>
            )}))

    return (
        <div>
            <h1>Profile - {name}</h1>
            <h2>Select An Album</h2>
            {albumLinks}
        </div>
    )
}

export default UserPage