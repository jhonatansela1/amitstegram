import { useState } from "react"
import { useFetchPhoto } from "../../hooks/fetchPhoto"
import Photo from "../Photo/Photo"
import Styles from './Album.module.css'

const Album = (props) => {

    const {id, title} = props

    const [photosList, setPhotosList] = useState([])
    const [isShown, setIsShown] = useState(false)

    useFetchPhoto(setPhotosList, id)


    const photos = (
        photosList.map((element, index) => {  
            return (
                    <div key={index}>
                        {isShown && <Photo url={element.url} title={element.title} />}
                        
                    </div>
            )
        })
    )

    return (
        <div>
            <button onClick={() => {setIsShown(!isShown)}}>{id} {title}<span></span><span></span><span></span><span></span></button>
            
            {photos}
        </div>
    )
}

export default Album