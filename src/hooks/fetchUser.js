import axios from "axios";
import { useEffect } from "react";



export const useFetchUser = async (setUsersList) => {
    useEffect(() => {
        async function getUsers(){
            try{
                const {data} = await axios.get("https://jsonplaceholder.typicode.com/users")
                setUsersList(data)
            }
            catch(err){
                console.error(err)
            }
        }
        getUsers()
    }, [])
}