import axios from "axios";
import { useEffect } from "react";



export const useFetchAlbums = async (id, setAlbumsList) => {
    useEffect(() => {
        async function getAlbums(){
            try{
                const {data} = await axios.get(`https://jsonplaceholder.typicode.com/albums?userId=${id}`)
                setAlbumsList(data)
            }
            catch(err){
                console.error(err)
            }
        }
        getAlbums()
    },[])
}