import axios from "axios";
import { useEffect } from "react";



export const useFetchAllAlbums = async (setAllAlbumsList) => {
    useEffect(() => {
        async function getAllAlbums(){
            try{
                const {data} = await axios.get(`https://jsonplaceholder.typicode.com/albums`)
                setAllAlbumsList(data)
                console.log(data)
            }
            catch(err){
                console.error(err)
            }
        }
        getAllAlbums()
    },[])
}