import axios from "axios";
import { useEffect } from "react";


export const useFetchPhoto = async (setPhotoList, id) => {
    useEffect(() => {
        async function getPhoto(){
            try{
                const {data} = await axios.get(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`)
                setPhotoList(data)
            }
            catch(err){
                console.error(err)
            }
        }
        getPhoto()
    },[])
}